<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>HOME</title>
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
 <div id="con">
     <?php
     include 'menu.php';
     ?>
     <div id="daftar">
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Melihat Data Siswa</h5>
                            <p>untuk melihat data siswa, silahkan mennuju menu TKJ1 atau TKJ 2 disamping tombol peunjuk, atau menggunakan tombol cari yang ada dibawah menu login</p>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Untuk Orang tua</h5>
                            <p>jika ingin mengijinkan anaknya silahkan login dengan username dan password, setelah itu silahkan memilih menu ijin</p>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Untuk Guru</h5>
                            <p>jika ingin melakukan Presensi Siswa, silahkan login terlebih dahulu, kemudian memilih menu presensi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<?php
include 'menu-kanan.php';
?>
</body>
</html>
