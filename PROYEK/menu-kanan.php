<div id="sec">
   	<div id="kanan">
  		<form class="form-horizontal m-t-20" id="loginform" action="dbms/proses.php?ak=login" method="post">
            <div class="row p-b-30" >
                <div class="col-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
           	                <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                        </div>
                        <input name="user" type="text" class="form-control form-control-lg" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" required="">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-info text-white" id="basic-addon2"><i class="fas fa-lock"></i></span>
                        </div>
                    	<input name="pass" type="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
                    </div>
                    <?php if (isset($_GET['eror'])=='true') { ?>
                        <div style="color: red;font-style: italic;">
                            USERNAME / PASSWORD SALAH!!!
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="p-t-20">
                            <input class="btn btn-success" type="submit" value="Masuk">
                        </div>
                    </div>
                </div>
            </div>
        </form>
  	</div>
   	<div id="cari">
   		<form class="form-horizontal m-t-20" id="loginform" action="data.php" method="post">
            <div class="row p-b-30" >
                <div class="col-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
          	                <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                        </div>
                        <input name="nama" type="text" class="form-control form-control-lg" placeholder="nama" aria-label="Username" aria-describedby="basic-addon1" required="">
                        <input class="btn btn-success" type="submit" value="CARI">
                    </div>
                </div>
            </div>
        </form>
  	</div>
</div>