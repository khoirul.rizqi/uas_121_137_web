<?php 
include 'koneksi.php';
$ak = $_GET['ak'];
$data = new user();
$i = new ijin();
$pre = new absen();

if ($ak == 'login') {
	$ud = $data->getlog($_POST['user'],$_POST['pass']);
	if ($ud=='admin') {
		$data->ia($_POST);
		header("location:../admin/ltr/");
	}
	else if ($ud=='guru') {
		$data->ia($_POST);
		header("location:../guru/ltr/");
	}
	else if ($ud=='ortu') {
		$data->ia($_POST);
		header("location:../ortu/ltr/");
	}
	else{
		header("location:../index.php?eror=true");
	}
}
elseif($ak == 'up'){
	$data->updatedata($_POST['nama'],$_POST['user'],$_POST['pass'],$_GET['i'],$_GET['a'],$_POST['info']);
	$data->updatedata($_POST['nama'],$_POST['user'],$_POST['pass'],$_GET['i'],$_GET['a1'],$_POST['info']);
	if($_GET['a']=="admin"){
		header("location:../admin/ltr/profil.php");
	}
	else if ($_GET['a']=="guru") {
		header("location:../admin/ltr/aadmin.php");
	}
	else if ($_GET['a']=="ortu") {
		header("location:../admin/ltr/aortu.php");
	}
	else if ($_GET['a1']=="guru") {
		header("location:../guru/ltr/profil.php");
	}
	else if ($_GET['a1']=="ortu") {
		header("location:../ortu/ltr/profil.php");
	}
}
elseif($ak == 'ijin'){
	echo $_POST['ortu']." orang tua <br>";	//menmapilkan id orang tua
	echo $_POST['siswa']." siswa <br>";	//menampilkan id siswa
	echo $_POST['ket']." keterangan <br>"; //menampilkan keterangan ijin
	echo $_POST['ijin']." ijin <br>"; //isi dari ijin
	echo DATE('Y-m-d')." tanggal <br>"; //tanggal hari inni
	$t=DATE('Y-m-d'); //mengambil data tanggal hari ini
	$a = $pre->ik($_POST['siswa']); //mengambil data kelasid
	$x = $pre->ai($t,$a); //mengambil data absenid
	echo $a." kelasid <br>"; //menampilkan kelas
	echo $x." absenid <br>"; //menampilkan absen id
	if ($a=='TKJ01') { //mendeteksi kelas mana
		echo "<br>".$a;
		if (is_null($x)) { //cek isi pada x kosong atau tidak
			echo "<br> Tanggal di Absen Kosong";
			echo $t;
			$pre->isitgl1($t,$a);
			ia1();
		}
		else{
			echo $x;
			echo "<br> Tanggal di Absen Ada";
			ia1();
		}
	}
	else if ($a=='TKJ02') {
		echo "<br>".$a;
		if (is_null($x)) { //cek isi pada x kosong atau tidak
			echo "<br> Tanggal di Absen Kosong";
			echo $t;
			$pre->isitgl1($t,$a);
			ia2();
		}
		else{
			echo $x;
			echo "<br> Tanggal di Absen Ada";
			ia2();
		}
	}
	/*$i->tdata(
		$_POST['ortu'],
		$_POST['siswa'],
		$_POST['ket'],
		$_POST['ijin']
	);*/
	header("location:../ortu/ltr/ijin.php");
}
elseif ($ak == 'absen1') {
	$a = $_GET['tgl'];
	$t = $_GET['tgl'];
	$x = $pre->tgl1($t,'TKJ01'); //mengambil data tanggal
	echo $x.'<br>';
	if ($a==$x) {
		//echo "ADA<br>";
		//echo 'Tanggal <br>'.$x.'<br>';
		//pos($_GET['p']);
		isiabsen1();
		header("location:../guru/ltr/ptkj1.php");
	}
	else{
		//echo "KOSONG";
		//echo $x.'<br>';
		$pre->isitgl1($t,'TKJ01');
		isiabsen1();
		header("location:../guru/ltr/ptkj1.php");
	}
}
elseif ($ak == 'absen2') {
	$a = $_GET['tgl'];
	$t = $_GET['tgl'];
	$x = $pre->tgl1($t,'TKJ02');
	echo $x.'<br>';
	if ($a==$x) {
		//echo "ADA<br>";
		//echo 'Tanggal <br>'.$x.'<br>';
		isiabsen2();
		//pos($_GET['p']);
		header("location:../guru/ltr/ptkj2.php");
	}
	else{
		//echo "KOSONG";
		//echo $x.'<br>';
		$pre->isitgl1($t,'TKJ02');
		isiabsen2();
		header("location:../guru/ltr/ptkj2.php");
	}
}
elseif ($ak == 'reg') {
	$x = new regist();
	echo "string"."<br>";
	echo $_POST['nortu']."<br>";
	echo $_POST['uortu']."<br>";
	echo $_POST['portu']."<br>";
	echo $_POST['siswa']."<br>";
	echo $_POST['jurusan']."<br>";
	echo $_POST['inortu']."<br>";
	$x->inuser($_POST['uortu'],$_POST['portu']);
	$id = $x->iduser($_POST['uortu'],$_POST['portu']);
	$x->inortu($id,$_POST['nortu'],$_POST['inortu']);
	$ido = $x->idortu($_POST['nortu'],$_POST['inortu']);
	$x->insiswa($ido,$_POST['jurusan'],$ido,$_POST['siswa']);
	header("location:../admin/ltr/aortu.php");
}
elseif ($ak=='out') {
	session_destroy();
	session_unset();
	header("location:../");
	exit;
}
function isiabsen1(){
	$pre=new absen();
	$a = $_GET['tgl'];
	$t = $_GET['tgl'];
	$x = $pre->tgl1($t,'TKJ01');
	$id=$pre->gtgl1($t,'TKJ01');
	echo 'Id absen <br>'.$id.'<br>';
	echo 'Id Siswa <br>'.$_GET['i'].'<br>';
	echo 'Keterangan <br>'.$_GET['p'].'<br>';
	if ($pre->cekijin($_GET['i'])=='Ada') {
		echo "Ada ijin";
	}
	elseif($pre->cekijin($_GET['i'])=='Tidak') {
		echo "Tidak ada ijin";
		if ($_GET['p']=='masuk') {
			echo "MASUK";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'0','0','0');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'0','0','0');
			}
		}
		elseif ($_GET['p']=='ijin') {
			echo "I";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'0','1','0');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'0','1','0');
			}
		}
		elseif ($_GET['p']=='alasan') {
			echo "A";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'1','0','0');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'1','0','0');
			}
		}
		elseif ($_GET['p']=='sakit') {
			echo "S";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'0','0','1');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'0','0','1');
			}
		}
	}
}
function isiabsen2(){
	$pre = new absen();
	$a = $_GET['tgl'];
	$t = $_GET['tgl'];
	$x = $pre->tgl1($t,'TKJ02');
	$id=$pre->gtgl1($t,'TKJ02');
	echo 'Id absen <br>'.$id.'<br>';
	echo 'Id Siswa <br>'.$_GET['i'].'<br>';
	echo 'Keterangan <br>'.$_GET['p'].'<br>';
	if ($pre->cekijin($_GET['i'])=='Ada') {
		echo "Ada ijin";
	}
	elseif($pre->cekijin($_GET['i'])=='Tidak') {
		echo "Tidak ada ijin";
		if ($_GET['p']=='masuk') {
			echo "MASUK";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'0','0','0');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'0','0','0');
			}
		}
		elseif ($_GET['p']=='ijin') {
			echo "I";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'0','1','0');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'0','1','0');
			}
		}
		elseif ($_GET['p']=='alasan') {
			echo "A";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'1','0','0');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'1','0','0');
			}
		}
		elseif ($_GET['p']=='sakit') {
			echo "S";
			if ($pre->cekabsen($id,$_GET['i'])==$id) {
				echo "Tidak Absen";
				$pre->uabsen1($id,$_GET['i'],$_GET['p'],'0','0','1');
			}
			elseif ($pre->cekabsen($id,$_GET['i'])!=$id) {
				echo "Absen";
				$pre->iabsen1($id,$_GET['i'],$_GET['p'],'0','0','1');
			}
		}
	}
}
function ia1(){
	echo "<br>Mengisi Absen TKJ01";
	if ($_POST['ijin']=='Ijin') {
		echo "<br>ijin";
		ma("ijin");
	}
	elseif ($_POST['ijin']=='Sakit') {
		echo "<br>sakit";
		ma("sakit");
	}
}
function ia2(){
	echo "<br>Mengisi Absen TKJ02";
	if ($_POST['ijin']=='Ijin') {
		echo "<br>ijin";
		ma("ijin");
	}
	elseif ($_POST['ijin']=='Sakit') {
		echo "<br>sakit";
		ma("sakit");
	}
}
function ma($g){
	$pre=new absen();
	$t=DATE('Y-m-d'); //mengambil data tanggal hari ini
	$a = $pre->ik($_POST['siswa']); //mengambil data kelasid
	$x = $pre->ai($t,$a); //mengambil data absenid
	if ($pre->cekabsen($x,$_POST['siswa'])==$x) {
		echo "<br> Absen ADA, Tinggal Update".$a.",".$x.$g.$_POST['ket'];
		$pre->uijin($x,$_POST['siswa'],$g,$_POST['ket'],$_POST['ortu']);
	}
	elseif ($pre->cekabsen($x,$_POST['siswa'])!=$x) {
		echo "<br> Absen belum ADA, Tinggal Isi Dulu".$a.",".$x;
		$pre->iai($x,$_POST['siswa'],$g,$_POST['ket'],$_POST['ortu']);
	}	//mengisi/update detailabsen dan mengsisi/update ijin
}
?>