<div class="scroll-sidebar">
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav" class="p-t-30">
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-face"></i><span class="hide-menu">Akun</span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item"><a href="aadmin.php" class="sidebar-link"><i class="mdi mdi-emoticon"></i><span class="hide-menu">Admin</span></a></li>
                    <li class="sidebar-item"><a href="aguru.php" class="sidebar-link"><i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">Guru</span></a></li>
                    <li class="sidebar-item"><a href="aortu.php" class="sidebar-link"><i class="mdi mdi-emoticon-cool"></i><span class="hide-menu">Orang Tua</span></a></li>
                </ul>
            </li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="ijin.php" aria-expanded="false"><i class="mdi mdi-pencil"></i><span class="hide-menu">Daftar Ijin</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark sidebar-link" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Daftar Siswa</span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item"><a href="daftar.php" class="sidebar-link"><i class="mdi mdi-account-box"></i><span class="hide-menu">Registrasi</span></a></li>
                    <li class="sidebar-item"><a href="TKJ1.php" class="sidebar-link"><i class="mdi mdi-account-box"></i><span class="hide-menu">TKJ 01</span></a></li>
                    <li class="sidebar-item"><a href="TKJ2.php" class="sidebar-link"><i class="mdi mdi-account-box"></i><span class="hide-menu">TKJ 02</span></a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>