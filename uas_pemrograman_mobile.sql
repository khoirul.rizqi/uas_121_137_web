/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.11-MariaDB : Database - uas_pemrograman_mobile
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uas_pemrograman_mobile` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `uas_pemrograman_mobile`;

/*Table structure for table `absen` */

DROP TABLE IF EXISTS `absen`;

CREATE TABLE `absen` (
  `absenid` int(11) NOT NULL AUTO_INCREMENT,
  `kelasid` varchar(20) DEFAULT NULL,
  `guruid` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`absenid`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `absen` */

insert  into `absen`(`absenid`,`kelasid`,`guruid`,`tanggal`) values 
(1,'TKJ01',NULL,'2020-04-30'),
(2,'TKJ02',NULL,'2020-04-29'),
(3,'TKJ02',NULL,'2020-04-30'),
(4,'TKJ01',NULL,'2020-05-02'),
(5,'TKJ02',NULL,'2020-05-02'),
(6,'TKJ01',NULL,'2020-05-03'),
(7,'TKJ02',NULL,'2020-05-03'),
(8,'TKJ01',NULL,'2020-05-08'),
(9,'TKJ02',NULL,'2020-05-08'),
(10,'TKJ01',NULL,'2020-05-09'),
(11,'TKJ01',NULL,'2020-05-13'),
(14,'TKJ01',NULL,'2020-05-18'),
(15,'TKJ02',NULL,'2020-05-18'),
(16,'TKJ01',NULL,'2020-05-19');

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `userid` int(11) NOT NULL,
  `adminid` int(11) NOT NULL AUTO_INCREMENT,
  `adminnama` varchar(40) DEFAULT NULL,
  `admininfo` text DEFAULT NULL,
  PRIMARY KEY (`adminid`),
  KEY `dari` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `admin` */

insert  into `admin`(`userid`,`adminid`,`adminnama`,`admininfo`) values 
(1,1,'as','asa'),
(2,2,'Khoi','085706291308');

/*Table structure for table `detailabsen` */

DROP TABLE IF EXISTS `detailabsen`;

CREATE TABLE `detailabsen` (
  `absenid` int(11) DEFAULT NULL,
  `siswaid` int(11) DEFAULT NULL,
  `keterangan` enum('ijin','sakit','alasan','masuk') DEFAULT NULL,
  `A` int(11) DEFAULT NULL,
  `I` int(11) DEFAULT NULL,
  `S` int(11) DEFAULT NULL,
  KEY `fk_absen` (`absenid`),
  KEY `fk_siswa` (`siswaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `detailabsen` */

insert  into `detailabsen`(`absenid`,`siswaid`,`keterangan`,`A`,`I`,`S`) values 
(1,1,'masuk',0,0,0),
(2,2,'masuk',0,0,0),
(2,4,'alasan',1,0,0),
(1,3,'alasan',1,0,0),
(3,2,'ijin',0,1,0),
(4,1,'alasan',1,0,0),
(5,4,'alasan',1,0,0),
(9,4,'alasan',1,0,0),
(8,1,'sakit',0,0,1),
(8,3,'ijin',0,1,0),
(9,2,'sakit',0,0,1),
(10,1,'ijin',0,1,0),
(11,1,'sakit',0,0,1),
(0,3,'masuk',0,0,0),
(15,3,'masuk',0,0,0),
(15,5,'masuk',0,0,0),
(14,1,'alasan',1,0,0),
(16,3,'masuk',0,0,0),
(16,5,'masuk',0,0,0);

/*Table structure for table `guru` */

DROP TABLE IF EXISTS `guru`;

CREATE TABLE `guru` (
  `userid` int(11) NOT NULL,
  `guruid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(40) DEFAULT NULL,
  `gurunama` varchar(40) DEFAULT NULL,
  `gurumapel` varchar(40) DEFAULT NULL,
  `guruinfo` text DEFAULT NULL,
  PRIMARY KEY (`guruid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `guru` */

insert  into `guru`(`userid`,`guruid`,`nip`,`gurunama`,`gurumapel`,`guruinfo`) values 
(3,1,'12345678','Heru','Sistem Operasi','OKay'),
(4,2,'12345677','Nety','Web',NULL);

/*Table structure for table `ijin` */

DROP TABLE IF EXISTS `ijin`;

CREATE TABLE `ijin` (
  `idpesan` int(11) NOT NULL AUTO_INCREMENT,
  `siswaid` int(11) DEFAULT NULL,
  `ortuid` int(11) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `keterangan` enum('Ijin','Sakit') DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`idpesan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ijin` */

insert  into `ijin`(`idpesan`,`siswaid`,`ortuid`,`isi`,`keterangan`,`tanggal`) values 
(1,1,1,'','Ijin',NULL),
(2,1,1,'asaa','Sakit','2020-05-02'),
(3,2,2,'Sakit Karena Tugas','Sakit','2020-05-08'),
(4,1,1,'sakit','','2020-05-08'),
(5,1,1,'xfxdgx','Ijin','2020-05-09'),
(6,1,1,'sakit','','2020-05-13');

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `kelasid` varchar(20) NOT NULL,
  `kelasnama` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`kelasid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `kelas` */

insert  into `kelas`(`kelasid`,`kelasnama`) values 
('TKJ01','Teknik Komputer dan Jaringan 01'),
('TKJ02','Teknik Komputer dan Jaringan 02');

/*Table structure for table `orangtua` */

DROP TABLE IF EXISTS `orangtua`;

CREATE TABLE `orangtua` (
  `userid` int(11) DEFAULT NULL,
  `ortuid` int(11) NOT NULL AUTO_INCREMENT,
  `ortunama` varchar(40) DEFAULT NULL,
  `ortuinfo` text DEFAULT NULL,
  PRIMARY KEY (`ortuid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `orangtua` */

insert  into `orangtua`(`userid`,`ortuid`,`ortunama`,`ortuinfo`) values 
(5,1,'Siti','Okay'),
(6,2,'Yusuf',''),
(7,3,'Dewa',''),
(8,4,'rahma',''),
(9,5,'Almir','Kosong');

/*Table structure for table `siswa` */

DROP TABLE IF EXISTS `siswa`;

CREATE TABLE `siswa` (
  `siswaid` int(11) NOT NULL AUTO_INCREMENT,
  `kelasid` varchar(20) DEFAULT NULL,
  `ortuid` int(11) DEFAULT NULL,
  `namasiswa` varchar(40) DEFAULT NULL,
  `usersiswa` varchar(40) DEFAULT NULL,
  `passsiswa` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`siswaid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `siswa` */

insert  into `siswa`(`siswaid`,`kelasid`,`ortuid`,`namasiswa`,`usersiswa`,`passsiswa`) values 
(1,'TKJ01',1,'Amalia','Amalia','Amalia'),
(2,'TKJ02',2,'AHMAD YUSUF','Yusuf','Yusuf'),
(3,'TKJ01',3,'ALAYGRA YUSRA DEWANO','Alay','Alay'),
(4,'TKJ02',4,'TRI ULFA RAHMAWATI','Tri','Tri'),
(5,'TKJ01',5,'Almira','Almira','Almira');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `level` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`iduser`,`username`,`password`,`level`) values 
(1,'erlang','erlang','admin'),
(2,'khoirul','khoirul','admin'),
(3,'Heru','Heru','guru'),
(4,'Nety','Nety','guru'),
(5,'Siti','Siti','ortu'),
(6,'yusuf','yusuf','ortu'),
(7,'dewa','dewa','ortu'),
(8,'rahma','rahma','ortu'),
(9,'Mira','Mira','ortu');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
